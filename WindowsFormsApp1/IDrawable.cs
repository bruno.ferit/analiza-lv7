﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    interface IDrawable
    {
        void Draw(Pen p, Graphics g, int x, int y);
    }

    class Circle : IDrawable
    {
        private int r = 10;

        public void Draw(Pen p, Graphics g, int x, int y)
        {
            g.DrawEllipse(p, x, y, r, r);
        }
    }
}
