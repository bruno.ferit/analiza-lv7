﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Graphics g;
        Pen p;
        IDrawable drawable;

        public Form1()
        {
            InitializeComponent();
            this.g = pbDrawing.CreateGraphics();
            this.p = new Pen(Color.Red, 1.0f);
            this.drawable = new Circle();
        }

        private void pbDrawing_MouseUp(object sender, MouseEventArgs e)
        {
            drawable.Draw(p, g, e.X, e.Y);
        }

        private void color_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRed.Checked) p.Color = Color.Red;
            else if (rbBlue.Checked) p.Color = Color.BlueViolet;

        }
    }
}
